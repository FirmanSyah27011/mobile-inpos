import 'package:dio/dio.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:inpos/models/anak.dart';
import 'package:inpos/models/gizi.dart';
import 'package:inpos/services/api_service.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormAddGizi extends StatefulWidget {
  @override
  _FormAddGiziState createState() => _FormAddGiziState();
}

class _FormAddGiziState extends State<FormAddGizi> {
  String _asiEks, _vitA, _validasi;
  int _valAnak, _posyandu;
  final _formAddGizi = GlobalKey<FormState>();
  final keyAnak = GlobalKey<FormFieldState>();
  final keyBB = GlobalKey<FormFieldState>();
  final keyTB = GlobalKey<FormFieldState>();
  final keyAsi = GlobalKey<FormFieldState>();
  final keyVitA = GlobalKey<FormFieldState>();
  final keyValidasi = GlobalKey<FormFieldState>();

  List<dynamic> dataAnak = List.empty();

  TextEditingController txtAnak = TextEditingController();
  TextEditingController txtBB = TextEditingController();
  TextEditingController txtTB = TextEditingController();
  TextEditingController txtTgl_periksa = TextEditingController();
  _getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      _posyandu = pref.getInt('id_posyandu');
    });
  }

  void getAnak() async {
    final response = await http.get(ApiService.getAnak(_posyandu));
    //var listDataAnak = jsonDecode(response.body);
    if (response.body.isNotEmpty) {
      setState(() {
        dataAnak = jsonDecode(response.body);
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getAnak();
    _getPref();
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xff205072),
            ),
            onPressed: () {
              Get.back();
            },
          ),
          title: Text(
            "Form Tambah Gizi",
            style: TextStyle(color: Color(0xff205072)),
          ),
          backgroundColor: Color(0xffe0ecde),
        ),
        body: Form(
          key: _formAddGizi,
          child: ListView(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(20, 20, 20, 40),
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 10),
                    ),
                    DropdownSearch<Anak>(
                      key: keyAnak,
                      mode: Mode.MENU,
                      hint: "Nama anak",
                      label: "Pilih Anak",
                      onFind: (String filter) async {
                        var response = await Dio().get(
                          ApiService.getAnak(_posyandu),
                          queryParameters: {"filter": filter},
                        );
                        var models = Anak.fromJsonList(response.data);
                        return models;
                      },
                      validator: (Anak data) =>
                          data == null ? "Data anak tidak boleh kosong" : null,
                      autoValidateMode: AutovalidateMode.onUserInteraction,
                      itemAsString: (Anak u) => u.anakAsString(),
                      onChanged: (Anak data) {
                        _valAnak = data.idAnak;
                        print(data);
                      },
                      isFilteredOnline: true,
                      showSearchBox: true,
                      searchBoxDecoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                    ),
                    TextFormField(
                      key: keyTB,
                      controller: txtTB,
                      validator: RequiredValidator(
                        errorText: "Tinggi badan harus diisi!",
                      ),
                      keyboardType: TextInputType.numberWithOptions(
                        decimal: true,
                      ),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        hintText: "Input Tinggi Anak",
                        labelText: "Tinggi Anak",
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                    ),
                    TextFormField(
                      key: keyBB,
                      controller: txtBB,
                      validator: RequiredValidator(
                        errorText: "Berat badan harus diisi!",
                      ),
                      keyboardType: TextInputType.numberWithOptions(
                        decimal: true,
                      ),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        hintText: "Input Berat Anak",
                        labelText: "Berat Anak",
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text("ASI Eksklusif"),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: FormBuilderRadioGroup(
                            key: keyAsi,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                            ),
                            validators: [
                              FormBuilderValidators.required(
                                  errorText: "ASI Ekslusif harus diisi!")
                            ],
                            attribute: "ASI Eksklusif",
                            options: ["1", "2"]
                                .map((asi) => FormBuilderFieldOption(
                                      value: asi,
                                      child: Text("$asi"),
                                    ))
                                .toList(),
                            onChanged: (value) {
                              setState(() {
                                _asiEks = value;
                                print(_asiEks);
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text("Vitamin A"),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: FormBuilderRadioGroup(
                            key: keyVitA,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                            ),
                            validators: [
                              FormBuilderValidators.required(
                                errorText: "Vitamin A harus diisi!",
                              )
                            ],
                            attribute: "Vitamin A",
                            options: ["1", "2"]
                                .map((vit) => FormBuilderFieldOption(
                                      value: vit,
                                      child: Text("$vit"),
                                    ))
                                .toList(),
                            onChanged: (value) {
                              setState(() {
                                _vitA = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text("Validasi"),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: FormBuilderRadioGroup(
                            key: keyValidasi,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                            ),
                            validators: [
                              FormBuilderValidators.required(
                                errorText: "Validasi harus diisi!",
                              )
                            ],
                            attribute: "Validasi",
                            options: ["1", "2"]
                                .map((val) => FormBuilderFieldOption(
                                      value: val,
                                      child: Text("$val"),
                                    ))
                                .toList(),
                            onChanged: (value) {
                              setState(() {
                                _validasi = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          width: 150,
                          height: 50,
                          child: RaisedButton(
                            onPressed: () {
                              if (_formAddGizi.currentState.validate()) {
                                GiziController.createGizi(
                                  context,
                                  _valAnak,
                                  txtTB.text,
                                  txtBB.text,
                                  _asiEks,
                                  _vitA,
                                  _validasi,
                                  _posyandu,
                                );
                              }
                              print(txtTB.text);
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(80.0),
                            ),
                            padding: EdgeInsets.all(5),
                            color: Color(0xff205072),
                            elevation: 5,
                            child: Text(
                              "Simpan Data",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 18),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
