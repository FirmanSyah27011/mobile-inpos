import 'dart:async';

import 'package:flutter/material.dart';
import 'package:division/division.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:indonesia/indonesia.dart';
import 'package:inpos/models/gizi.dart';
import 'package:shimmer/shimmer.dart';
import 'package:auto_size_text/auto_size_text.dart';

class CardDetailGizi extends StatefulWidget {
  final ParentStyle cardStyle;
  final String id;
  CardDetailGizi(this.id, this.cardStyle);

  @override
  CardDetailGiziState createState() => CardDetailGiziState();
}

class CardDetailGiziState extends State<CardDetailGizi>
    with TickerProviderStateMixin {
  bool _enable = false;
  Timer _timer;

  @override
  void initState() {
    super.initState();
    _timer = Timer(Duration(seconds: 1), () {
      setState(() {
        _enable = true;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Parent(
      child: FutureBuilder<List<Gizi>>(
        future: GiziController.getDetailGizi(widget.id),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Gizi> data = snapshot.data;
            return _enable
                ? _gizi(data)
                : Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[100],
                    child: _gizi(data),
                  );
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          return SpinKitCubeGrid(
            color: Colors.blue,
            size: 30.0,
            controller: AnimationController(
              vsync: this,
              duration: const Duration(seconds: 3),
            ),
          );
        },
      ),
    );
  }

  ListView _gizi(data) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: data.length,
        itemBuilder: (context, index) {
          return Parent(
            style: widget.cardStyle,
            child: Column(
              children: [
                _tile(
                  data[index].noPemeriksaanGizi,
                  data[index].anak.nik,
                  data[index].anak.namaAnak,
                  data[index].usia,
                  data[index].anak.jk,
                  data[index].tglPeriksa,
                  data[index].pbTb,
                  data[index].bb,
                  data[index].vitA,
                  data[index].statusGizi.bbu,
                  data[index].statusGizi.pbtbu,
                  data[index].statusGizi.bbpbtb,
                ),
              ],
            ),
          );
        });
  }

  ListTile _tile(
          String id,
          String nik,
          String anak,
          String usia,
          String jk,
          String tglUkur,
          int pbTb,
          double bb,
          String vitA,
          String bbu,
          String pbtbu,
          String bbpbtb) =>
      ListTile(
        title: Container(
          margin: EdgeInsets.only(top: 15, bottom: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                title: Text(
                  "No",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 15),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    '$id',
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Nama",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 15),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    '$anak',
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Usia",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 15),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    '$usia Bulan',
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Jenis Kelamin",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    "$jk",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Tanggal",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    tanggal(DateTime.parse(tglUkur)),
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 14),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Berat Badan",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    "$bb Kg",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Panjang Badan",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    "$pbTb Cm",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Vitamin A",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    vitA == '1' ? "Ya" : "Tidak",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 10),
                child: Center(
                  child: Text(
                    "Status Gizi",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20),
                  ),
                ),
              ),
              ListTile(
                title: Text(
                  "BB / U",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    bbu,
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "PB_TB / U",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    pbtbu,
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "BB_PB / TB",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    bbpbtb,
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
