import 'dart:async';

import 'package:flutter/material.dart';
import 'package:division/division.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:inpos/models/gizi.dart';
import 'package:inpos/views/pages/gizi/detail_gizi.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:inpos/helper/screen.dart';

class GiziPage extends StatefulWidget {
  final ParentStyle cardStyle;
  GiziPage(this.cardStyle);
  @override
  _GiziPageState createState() => _GiziPageState();
}

class _GiziPageState extends State<GiziPage> with TickerProviderStateMixin {
  TextEditingController search = TextEditingController();
  String filter;
  String resultSearch = '';

  int _posyandu;
  bool _enable = false;
  Timer _timer;

  _getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      _posyandu = pref.getInt('id_posyandu');
    });
  }

  @override
  void initState() {
    super.initState();
    _getPref();
    _timer = Timer(Duration(seconds: 5), () {
      setState(() {
        _enable = true;
      });
    });
    search.addListener(() {
      setState(() {
        filter = search.text;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
    search.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Parent(
      child: ListView(
        children: [
          SizedBox(
            width: double.infinity,
            height: 160.0,
            child: Material(
              color: Color(0xffe0ecde),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(34),
                bottomRight: Radius.circular(34),
              ),
              child: Padding(
                padding: EdgeInsets.fromLTRB(25, 60, 25, 10),
                child: TextField(
                  controller: search,
                  decoration: InputDecoration(
                      suffixIcon: Icon(Icons.search),
                      filled: true,
                      fillColor: Colors.white,
                      hintText: 'Cari Data Gizi',
                      contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20))),
                ),
              ),
            ),
          ),
          FutureBuilder<List<Gizi>>(
            future: GiziController.getGizi(_posyandu),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<Gizi> data = snapshot.data;
                return _enable
                    ? _allGizi(data)
                    : Shimmer.fromColors(
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.grey[100],
                        child: _allGizi(data),
                      );
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              return Padding(
                padding: const EdgeInsets.fromLTRB(10, 250, 10, 10),
                child: SpinKitCubeGrid(
                  color: Colors.blue,
                  size: 30.0,
                  controller: AnimationController(
                    vsync: this,
                    duration: const Duration(milliseconds: 1900),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  ListView _allGizi(data) {
    return ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: data.length,
        itemBuilder: (context, index) {
          return filter == null || filter == ''
              ? Parent(
                  style: widget.cardStyle,
                  gesture: Gestures()
                    ..onTap(
                      () {
                        Get.to(
                          DetailGizi(gizi: data[index]),
                          transition: Transition.leftToRight,
                          duration: Duration(seconds: 1),
                        );
                      },
                    ),
                  child: Column(
                    children: [
                      _tile(data[index].noPemeriksaanGizi, data[index].anak.nik,
                          data[index].anak.namaAnak, data[index].anak.jk),
                    ],
                  ),
                )
              : '${data[index].noPemeriksaanGizi}'
                          '${data[index].anak.nik}'
                          '${data[index].anak.namaAnak}'
                          '${data[index].anak.jk}'
                      .toLowerCase()
                      .contains(filter.toLowerCase())
                  ? Parent(
                      style: widget.cardStyle,
                      gesture: Gestures()
                        ..onTap(
                          () {
                            Get.to(
                              DetailGizi(gizi: data[index]),
                              transition: Transition.upToDown,
                              duration: Duration(seconds: 1),
                            );
                          },
                        ),
                      child: Container(
                        //height: Screen.height(context),
                        width: Screen.width(context),
                        child: Column(
                          children: [
                            _tile(
                              data[index].noPemeriksaanGizi,
                              data[index].anak.nik,
                              data[index].anak.namaAnak,
                              data[index].anak.jk,
                            ),
                          ],
                        ),
                      ),
                    )
                  : Container();
        });
  }

  ListTile _tile(String id, String nik, String anak, String jk) => ListTile(
        title: Container(
          margin: EdgeInsets.only(top: 15, bottom: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                "No $id",
                style: TxtStyle()
                  ..margin(top: 5)
                  ..alignment.topLeft(),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 20,
                indent: 0,
                endIndent: 100,
              ),
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 5),
              ),
              Txt(
                nik != null ? nik : "-",
                style: TxtStyle()
                  ..margin(top: 5)
                  ..alignment.topLeft(),
              ),
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 5),
              ),
              Txt(
                "$anak | $jk",
                style: TxtStyle()
                  ..margin(top: 5)
                  ..alignment.topLeft(),
              ),
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 5),
              ),
            ],
          ),
        ),
      );
}
