import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:inpos/models/anak.dart';
import 'package:inpos/models/gizi.dart';
import 'package:inpos/services/api_service.dart';
import 'package:http/http.dart' as http;
import 'package:form_field_validator/form_field_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormEditGizi extends StatefulWidget {
  final Gizi gizi;
  const FormEditGizi({Key key, this.gizi}) : super(key: key);
  @override
  _FormEditGiziState createState() => _FormEditGiziState();
}

class _FormEditGiziState extends State<FormEditGizi> {
  String _asiEks, _vitA, _validasi, _namaAnak, _id;
  int _valAnak, _posyandu;
  final _formEditGizi = GlobalKey<FormState>();
  List<dynamic> dataAnak = List.empty();

  TextEditingController txtAnak = TextEditingController();
  TextEditingController txtBB = TextEditingController();
  TextEditingController txtTB = TextEditingController();

  _getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      _posyandu = pref.getInt('id_posyandu');
    });
  }

  void getAnak() async {
    final response = await http.get(ApiService.getAnak(_posyandu));
    var listDataAnak = jsonDecode(response.body);
    setState(() {
      dataAnak = listDataAnak;
    });
  }

  @override
  void initState() {
    super.initState();
    getAnak();
    _getPref();
    _valAnak = widget.gizi.idAnak;
    txtBB.text = widget.gizi.bb.toString();
    txtTB.text = widget.gizi.pbTb.toString();
    _asiEks = widget.gizi.asiEks;
    _vitA = widget.gizi.vitA;
    _validasi = widget.gizi.validasi;
    _namaAnak = widget.gizi.anak.namaAnak;
    _id = widget.gizi.noPemeriksaanGizi;
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xffe0ecde),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xff205072),
            ),
            onPressed: () {
              Get.back();
            },
          ),
          title: Text(
            "Form Edit Gizi",
            style: TextStyle(color: Color(0xff205072)),
          ),
        ),
        body: Form(
          key: _formEditGizi,
          child: ListView(
            children: [
              Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 40),
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      DropdownSearch<Anak>(
                        mode: Mode.MENU,
                        label: "Anak",
                        onFind: (String filter) async {
                          var response = await Dio().get(
                            ApiService.getAnak(_posyandu),
                            queryParameters: {"filter": filter},
                          );
                          var models = Anak.fromJsonList(response.data);
                          return models;
                        },
                        itemAsString: (Anak u) => u.anakAsString(),
                        onChanged: (Anak data) {
                          _valAnak = data.idAnak;
                          print(_namaAnak);
                        },
                        selectedItem:
                            Anak(idAnak: _valAnak, namaAnak: _namaAnak),
                        isFilteredOnline: true,
                        showSearchBox: true,
                        searchBoxDecoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                      ),
                      TextFormField(
                        controller: txtTB,
                        validator: RequiredValidator(
                            errorText: "Tinggi badan harus diisi!"),
                        keyboardType: TextInputType.numberWithOptions(
                          decimal: true,
                        ),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          labelText: "Tinggi Anak",
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                      ),
                      TextFormField(
                        controller: txtBB,
                        keyboardType: TextInputType.numberWithOptions(
                          decimal: true,
                        ),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          labelText: "Berat Anak",
                        ),
                      ),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text("ASI Eksklusif"),
                      ),
                      ButtonBar(
                        alignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Radio(
                            value: "1",
                            groupValue: _asiEks,
                            onChanged: (value) {
                              setState(() {
                                _asiEks = value;
                              });
                            },
                          ),
                          Text("Ya"),
                          Radio(
                            value: "2",
                            groupValue: _asiEks,
                            onChanged: (value) {
                              setState(() {
                                _asiEks = value;
                              });
                            },
                          ),
                          Text("Tidak"),
                        ],
                      ),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text("Vitamin A"),
                      ),
                      ButtonBar(
                        alignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Radio(
                            value: "1",
                            groupValue: _vitA,
                            onChanged: (value) {
                              setState(() {
                                _vitA = value;
                              });
                            },
                          ),
                          Text("Ya"),
                          Radio(
                            value: "2",
                            groupValue: _vitA,
                            onChanged: (value) {
                              setState(() {
                                _vitA = value;
                              });
                            },
                          ),
                          Text("Tidak"),
                        ],
                      ),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text("Validasi"),
                      ),
                      ButtonBar(
                        alignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Radio(
                            value: "1",
                            groupValue: _validasi,
                            onChanged: (value) {
                              setState(() {
                                _validasi = value;
                              });
                            },
                          ),
                          Text("Ya"),
                          Radio(
                            value: "2",
                            groupValue: _validasi,
                            onChanged: (value) {
                              setState(() {
                                _vitA = value;
                              });
                            },
                          ),
                          Text("Tidak"),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20),
                            width: 150,
                            height: 50,
                            child: RaisedButton(
                              onPressed: () {
                                if (_formEditGizi.currentState.validate()) {
                                  GiziController.updateGizi(
                                    context,
                                    _id,
                                    _valAnak,
                                    txtTB.text,
                                    txtBB.text,
                                    _asiEks,
                                    _vitA,
                                    _validasi,
                                  );
                                }
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(80.0),
                              ),
                              padding: EdgeInsets.all(5),
                              color: Color(0xff205072),
                              elevation: 5,
                              child: Text(
                                "Ubah Data",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
