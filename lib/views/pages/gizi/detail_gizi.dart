import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:inpos/models/gizi.dart';
import 'package:inpos/views/styles/style.dart';
import 'package:inpos/views/pages/gizi/form_edit_gizi.dart';
import 'card_detail_gizi.dart';

class DetailGizi extends StatelessWidget {
  final Gizi gizi;
  const DetailGizi({Key key, this.gizi}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xffe0ecde),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xff205072),
            ),
            onPressed: () {
              Get.back();
            },
          ),
          title: Text(
            "Detail Gizi",
            style: TextStyle(color: Color(0xff205072)),
          ),
        ),
        body: ListView(
          children: <Widget>[
            CardDetailGizi(gizi.noPemeriksaanGizi, Style.cardDetailStyle),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 10, bottom: 15),
                  width: 150,
                  height: 50,
                  child: RaisedButton(
                    onPressed: () {
                      Get.to(
                        FormEditGizi(gizi: gizi),
                        transition: Transition.downToUp,
                        duration: Duration(seconds: 1),
                      );
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80.0),
                    ),
                    padding: EdgeInsets.all(5),
                    color: Color(0xff205072),
                    elevation: 5,
                    child: Text(
                      "Edit Data",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
