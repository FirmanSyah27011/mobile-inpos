import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _name, _posyandu, _puskesmas, _alamat;
  _getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      _name = pref.getString('name');
      _posyandu = pref.getString('nama_posyandu');
      _puskesmas = pref.getString('nama_puskesmas');
      _alamat = pref.getString('alamat');
    });
  }

  @override
  void initState() {
    super.initState();
    _getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          SizedBox(
            width: double.infinity,
            height: 235.0,
            child: Material(
              color: Color(0xffe0ecde),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(34),
                bottomRight: Radius.circular(34),
              ),
              child: Container(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10, left: 10),
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(
                              right: 15,
                              bottom: 50,
                              top: 15,
                            ),
                            alignment: Alignment.centerRight,
                            child: CircleAvatar(
                              radius: 35.0,
                              backgroundColor: Colors.white,
                              child: CircleAvatar(
                                backgroundColor: Color(0xff205072),
                                radius: 30,
                                child: Padding(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: Text(_name != null ? _name[0] : "",
                                      style: TextStyle(
                                          fontSize: 50, color: Colors.white),
                                      textAlign: TextAlign.center),
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: AutoSizeText(
                              'Halo, Selamat Datang $_name',
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff205072),
                                  fontSize: 20),
                              maxFontSize: 20,
                              maxLines: 1,
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: AutoSizeText(
                              'Selalu menjaga pola pikir positif dan tersenyum.',
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: Color(0xff70af85),
                                  fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(5, 100, 5, 5),
            child: Card(
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(14)),
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.home),
                    title: Text(
                      'Posyandu $_posyandu',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.local_hospital),
                    title: Text(
                      'Puskesmas $_puskesmas',
                    ),
                  ),
                  Divider(
                    color: Color(0xff205072),
                    thickness: 2.0,
                    height: 20,
                    indent: 20,
                    endIndent: 100,
                  ),
                  ListTile(
                    leading: Icon(Icons.location_on),
                    title: Text(
                      '$_alamat',
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
