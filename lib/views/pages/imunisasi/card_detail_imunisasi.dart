import 'dart:async';

import 'package:flutter/material.dart';
import 'package:division/division.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:indonesia/indonesia.dart';
import 'package:inpos/models/imunisasi.dart';
import 'package:shimmer/shimmer.dart';
import 'package:auto_size_text/auto_size_text.dart';

class CardDetailImunisasi extends StatefulWidget {
  final ParentStyle cardStyle;
  final String id;
  CardDetailImunisasi(this.id, this.cardStyle);

  @override
  CardDetailImunisasiState createState() => CardDetailImunisasiState();
}

class CardDetailImunisasiState extends State<CardDetailImunisasi>
    with TickerProviderStateMixin {
  bool _enable = false;
  Timer _timer;

  @override
  void initState() {
    super.initState();
    _timer = Timer(Duration(seconds: 1), () {
      setState(() {
        _enable = true;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Parent(
      child: FutureBuilder<List<Imunisasi>>(
        future: ImunisasiController.getDetailImunisasi(widget.id),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Imunisasi> data = snapshot.data;
            return _enable
                ? _imunisasi(data)
                : Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[100],
                    child: _imunisasi(data),
                  );
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          return SpinKitCubeGrid(
            color: Colors.blue,
            size: 30.0,
            controller: AnimationController(
              vsync: this,
              duration: const Duration(seconds: 3),
            ),
          );
        },
      ),
    );
  }

  ListView _imunisasi(data) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: data.length,
        itemBuilder: (context, index) {
          return Parent(
            style: widget.cardStyle,
            child: Column(
              children: [
                _tile(
                  data[index].noPemeriksaanImunisasi,
                  data[index].tglImunisasi,
                  data[index].anak.nik,
                  data[index].anak.jk,
                  data[index].anak.tglLahir,
                  data[index].vaksinasi.namaVaksinasi,
                  data[index].anak.namaAnak,
                ),
              ],
            ),
          );
        });
  }

  ListTile _tile(
    String id,
    String tglImunisasi,
    String nik,
    String jk,
    String tglLahir,
    String vaksin,
    String anak,
  ) =>
      ListTile(
        title: Container(
          margin: EdgeInsets.only(top: 15, bottom: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                title: Text(
                  "No",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 15),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    '$id',
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Nama",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 15),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    '$anak',
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "NIK ",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 15),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    nik != null ? "$nik" : "-",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Jenis Kelamin",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    "$jk",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Tanggal",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    tanggal(DateTime.parse(tglImunisasi)),
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 14),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Jenis Vaksin",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    "$vaksin",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 14),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
