import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:inpos/models/anak.dart';
import 'package:inpos/models/imunisasi.dart';
import 'package:inpos/models/vaksinasi.dart';
import 'package:inpos/services/api_service.dart';
import 'package:http/http.dart' as http;
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:inpos/views/styles/style.dart';
import 'package:flutter/cupertino.dart';

import 'package:shared_preferences/shared_preferences.dart';

class FormAddImunisasi extends StatefulWidget {
  @override
  _FormAddImunisasiState createState() => _FormAddImunisasiState();
}

class _FormAddImunisasiState extends State<FormAddImunisasi> {
  int _valAnak, _valVaksinasi, _posyandu;

  final formAddImun = GlobalKey<FormState>();
  final keyAnak = GlobalKey<DropdownSearchState>();
  final keyVaksin = GlobalKey<DropdownSearchState>();

  List<dynamic> dataAnak = List.empty();
  List<dynamic> datavaksinasi = List.empty();

  void getVaksinasi() async {
    final response = await http.get(ApiService.getVaksinasi());
    //var listDataVaksinasi = jsonDecode(response.body);
    if (response.body.isNotEmpty) {
      setState(() {
        datavaksinasi = json.decode(response.body);
      });
    }
  }

  _getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      _posyandu = pref.getInt('id_posyandu');
    });
  }

  void getAnak() async {
    final response = await http.get(ApiService.getAnak(_posyandu));
    //var listDataAnak = jsonDecode(response.body);
    if (response.body.isNotEmpty) {
      setState(() {
        dataAnak = json.decode(response.body);
      });
    }
  }

  void initState() {
    super.initState();
    getAnak();
    getVaksinasi();
    _getPref();
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xff205072),
            ),
            onPressed: () {
              Get.back();
            },
          ),
          title: Text("Form Tambah Imunisasi",
              style: TextStyle(color: Color(0xff205072))),
          backgroundColor: Color(0xffe0ecde),
        ),
        body: Form(
          key: formAddImun,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: ListView(
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(10, 30, 10, 30),
                child: Column(
                  children: <Widget>[
                    DropdownSearch<Anak>(
                      key: keyAnak,
                      mode: Mode.MENU,
                      hint: "Nama anak",
                      label: "Pilih Anak",
                      onFind: (String filter) async {
                        var response = await Dio().get(
                          ApiService.getAnak(_posyandu),
                          queryParameters: {"filter": filter},
                        );
                        var models = Anak.fromJsonList(response.data);
                        return models;
                      },
                      itemAsString: (Anak u) => u.anakAsString(),
                      onChanged: (Anak dataAnak) {
                        _valAnak = dataAnak.idAnak;
                      },
                      isFilteredOnline: true,
                      showSearchBox: true,
                      searchBoxDecoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    ),
                    DropdownSearch<Vaksinasi>(
                      key: keyVaksin,
                      mode: Mode.MENU,
                      hint: "Nama vaksin",
                      label: "Pilih vaksin",
                      onFind: (String filter) async {
                        var response = await Dio().get(
                          ApiService.getVaksinasi(),
                          queryParameters: {"filter": filter},
                        );
                        var models = Vaksinasi.fromJsonList(response.data);
                        return models;
                      },
                      itemAsString: (Vaksinasi u) => u.vaksinasiAsString(),
                      onChanged: (Vaksinasi data) {
                        _valVaksinasi = data.idVaksinasi;
                      },
                      isFilteredOnline: true,
                      showSearchBox: true,
                      searchBoxDecoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          width: 150,
                          height: 50,
                          child: RaisedButton(
                            onPressed: () {
                              if (_valAnak == null || _valVaksinasi == null) {
                                Alert(
                                  context: context,
                                  title:
                                      "Data Anak dan Vaksinasi tidak boleh kosong",
                                  type: AlertType.error,
                                  style: Style.alertStyle,
                                ).show();
                                return;
                              }
                              if (formAddImun.currentState.validate()) {
                                ImunisasiController.createImunisasi(
                                  context,
                                  _valAnak,
                                  _valVaksinasi,
                                );
                              }
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(80.0),
                            ),
                            padding: EdgeInsets.all(5),
                            color: Color(0xff205072),
                            elevation: 5,
                            child: Text(
                              "Simpan Data",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
