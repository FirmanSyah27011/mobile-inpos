import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:inpos/models/anak.dart';
import 'package:inpos/models/imunisasi.dart';
import 'package:inpos/models/vaksinasi.dart';
import 'package:inpos/services/api_service.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class FormEditImunisasi extends StatefulWidget {
  final Imunisasi imunisasi;
  const FormEditImunisasi({Key key, this.imunisasi}) : super(key: key);
  @override
  _FormEditImunisasiState createState() => _FormEditImunisasiState();
}

class _FormEditImunisasiState extends State<FormEditImunisasi> {
  String _namaAnak, _namaVaksin, _id;
  int _valAnak, _valVaksin, _posyandu;
  final _formEditImun = GlobalKey<FormState>();
  List<dynamic> dataAnak = List.empty();
  List<dynamic> dataVaksin = List.empty();

  void getAnak() async {
    final response = await http.get(ApiService.getAnak(_posyandu));
    var listDataAnak = jsonDecode(response.body);
    setState(() {
      dataAnak = listDataAnak;
    });
  }

  void getVaksinasi() async {
    final response = await http.get(ApiService.getVaksinasi());
    var listDataVaksin = jsonDecode(response.body);
    setState(() {
      dataVaksin = listDataVaksin;
    });
  }

  _getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      _posyandu = pref.getInt('id_posyandu');
    });
  }

  @override
  void initState() {
    super.initState();
    getAnak();
    getVaksinasi();
    _getPref();
    _valAnak = widget.imunisasi.idAnak;
    _valVaksin = widget.imunisasi.vaksinasi.idVaksinasi;
    _namaAnak = widget.imunisasi.anak.namaAnak;
    _namaVaksin = widget.imunisasi.vaksinasi.namaVaksinasi;
    _id = widget.imunisasi.noPemeriksaanImunisasi;
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xffe0ecde),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xff205072),
            ),
            onPressed: () {
              Get.back();
            },
          ),
          title: Text(
            "Form Edit Imunisasi",
            style: TextStyle(color: Color(0xff205072)),
          ),
        ),
        body: Form(
          key: _formEditImun,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: ListView(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(20, 20, 20, 40),
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    DropdownSearch<Anak>(
                      mode: Mode.MENU,
                      label: "Anak",
                      onFind: (String filter) async {
                        var response = await Dio().get(
                          ApiService.getAnak(_posyandu),
                          queryParameters: {"filter": filter},
                        );
                        var models = Anak.fromJsonList(response.data);
                        return models;
                      },
                      validator: (Anak data) =>
                          data == null ? "Data anak tidak boleh kosong" : null,
                      autoValidateMode: AutovalidateMode.onUserInteraction,
                      itemAsString: (Anak u) => u.anakAsString(),
                      onChanged: (Anak data) {
                        _valAnak = data.idAnak;
                      },
                      selectedItem: Anak(idAnak: _valAnak, namaAnak: _namaAnak),
                      isFilteredOnline: true,
                      showSearchBox: true,
                      searchBoxDecoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                    ),
                    DropdownSearch<Vaksinasi>(
                      mode: Mode.MENU,
                      label: "Vaksin",
                      onFind: (String filter) async {
                        var response = await Dio().get(
                          ApiService.getVaksinasi(),
                          queryParameters: {"filter": filter},
                        );
                        var models = Vaksinasi.fromJsonList(response.data);
                        return models;
                      },
                      validator: (Vaksinasi data) => data == null
                          ? "Data vaksinasi tidak boleh kosong"
                          : null,
                      autoValidateMode: AutovalidateMode.onUserInteraction,
                      itemAsString: (Vaksinasi u) => u.vaksinasiAsString(),
                      onChanged: (Vaksinasi data) {
                        _valVaksin = data.idVaksinasi;
                        print(_namaVaksin);
                      },
                      selectedItem: Vaksinasi(
                          idVaksinasi: _valVaksin, namaVaksinasi: _namaVaksin),
                      isFilteredOnline: true,
                      showSearchBox: true,
                      searchBoxDecoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          width: 150,
                          height: 50,
                          child: RaisedButton(
                            onPressed: () {
                              if (_formEditImun.currentState.validate()) {
                                ImunisasiController.updateImunisasi(
                                  context,
                                  _id,
                                  _valAnak,
                                  _valVaksin,
                                );
                              }
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(80.0),
                            ),
                            padding: EdgeInsets.all(5),
                            color: Color(0xff205072),
                            elevation: 5,
                            child: Text(
                              "Ubah Data",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
