import 'dart:async';
import 'package:division/division.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:inpos/models/anak.dart';
import 'package:inpos/views/pages/anak/detail_anak.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:inpos/helper/screen.dart';
import 'package:auto_size_text/auto_size_text.dart';

class AnakPage extends StatefulWidget {
  final ParentStyle cardStyle;
  AnakPage(this.cardStyle);
  @override
  _AnakPageState createState() => _AnakPageState();
}

class _AnakPageState extends State<AnakPage> with TickerProviderStateMixin {
  TextEditingController search = TextEditingController();
  String filter;

  int _posyandu;
  bool _enable = false;
  Timer _timer;

  _getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      _posyandu = pref.getInt('id_posyandu');
    });
  }

  @override
  void initState() {
    super.initState();
    _getPref();

    _timer = Timer(Duration(seconds: 5), () {
      setState(() {
        _enable = true;
      });
    });
    search.addListener(() {
      setState(() {
        filter = search.text;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
    search.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Parent(
      child: ListView(
        children: [
          SizedBox(
            width: double.infinity,
            height: 160.0,
            child: Material(
              color: Color(0xffe0ecde),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(34),
                bottomRight: Radius.circular(34),
              ),
              child: Padding(
                padding: EdgeInsets.fromLTRB(25, 60, 25, 10),
                child: TextField(
                  controller: search,
                  decoration: InputDecoration(
                      suffixIcon: Icon(Icons.search),
                      filled: true,
                      fillColor: Colors.white,
                      hintText: 'Cari Data Anak',
                      contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20))),
                ),
              ),
            ),
          ),
          FutureBuilder<List<Anak>>(
            future: AnakController.getDataAnak(_posyandu),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<Anak> data = snapshot.data;
                return _enable
                    ? _allAnak(data)
                    : Shimmer.fromColors(
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.grey[100],
                        child: _allAnak(data),
                      );
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              return Padding(
                padding: const EdgeInsets.fromLTRB(10, 250, 10, 10),
                child: SpinKitCubeGrid(
                  color: Colors.blue,
                  size: 30.0,
                  controller: AnimationController(
                    vsync: this,
                    duration: const Duration(milliseconds: 1900),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  ListView _allAnak(data) {
    return ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: data.length,
        itemBuilder: (context, index) {
          return filter == null || filter == ''
              ? Parent(
                  style: widget.cardStyle,
                  gesture: Gestures()
                    ..onTap(
                      () {
                        Get.to(
                          DetailAnak(anak: data[index]),
                          transition: Transition.upToDown,
                          duration: Duration(seconds: 1),
                        );
                      },
                    ),
                  child: Container(
                    //height: Screen.height(context),
                    width: Screen.width(context),
                    child: Column(
                      children: [
                        _tile(
                          data[index].namaAnak,
                          data[index].nik,
                          data[index].jk,
                          data[index].keluarga.namaAyah,
                          data[index].keluarga.namaIbu,
                        ),
                      ],
                    ),
                  ),
                )
              : '${data[index].nik}'
                          '${data[index].namaAnak}'
                          '${data[index].jk}'
                          '${data[index].keluarga.namaAyah}'
                          '${data[index].keluarga.namaIbu}'
                      .toLowerCase()
                      .contains(filter.toLowerCase())
                  ? Parent(
                      style: widget.cardStyle,
                      gesture: Gestures()
                        ..onTap(
                          () {
                            Get.to(
                              DetailAnak(anak: data[index]),
                              transition: Transition.upToDown,
                              duration: Duration(seconds: 1),
                            );
                          },
                        ),
                      child: Container(
                        //height: Screen.height(context),
                        width: Screen.width(context),
                        child: Column(
                          children: [
                            _tile(
                              data[index].namaAnak,
                              data[index].nik,
                              data[index].jk,
                              data[index].keluarga.namaAyah,
                              data[index].keluarga.namaIbu,
                            ),
                          ],
                        ),
                      ),
                    )
                  : Container();
        });
  }

  ListTile _tile(String nama, String nik, String jk, String ayah, String ibu) =>
      ListTile(
        title: Container(
          margin: EdgeInsets.only(top: 15, bottom: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AutoSizeText(
                nik != null ? nik : "-",
                style: TextStyle(fontSize: 17),
                maxLines: 1,
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 20,
                indent: 0,
                endIndent: 50,
              ),
              Container(
                child: AutoSizeText(
                  nama + " | " + jk,
                  style: TextStyle(fontSize: 18),
                  maxLines: 1,
                ),
                margin: EdgeInsets.only(top: 5, bottom: 5),
              ),
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 5),
              ),
              Container(
                child: AutoSizeText(
                  "Orang tua $ayah - $ibu",
                  style: TextStyle(fontSize: 18),
                  maxLines: 1,
                ),
                margin: EdgeInsets.only(top: 5, bottom: 5),
              ),
            ],
          ),
        ),
      );
}
