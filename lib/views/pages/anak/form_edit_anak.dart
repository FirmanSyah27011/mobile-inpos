import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:inpos/models/anak.dart';
import 'package:inpos/models/desa.dart';
import 'package:inpos/services/api_service.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:form_field_validator/form_field_validator.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormEditAnak extends StatefulWidget {
  final Anak anak;
  const FormEditAnak({Key key, this.anak}) : super(key: key);

  @override
  _FormEditAnakState createState() => _FormEditAnakState();
}

class _FormEditAnakState extends State<FormEditAnak>
    with TickerProviderStateMixin {
  int _valDesa, _posyandu, _idAnak;
  String _gender, _kia, _imd, _ekonomi, namaDesa;
  final format = DateFormat("yyyy-MM-dd");
  final _formEditAnak = GlobalKey<FormState>();
  List<dynamic> dataDesa = List.empty();

  void getDesa() async {
    final response = await http.get(ApiService.getDesa());
    var listDataDesa = jsonDecode(response.body);
    print("data : $listDataDesa");
    setState(() {
      dataDesa = listDataDesa;
    });
  }

  _getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      _posyandu = pref.getInt('id_posyandu');
    });
  }

  TextEditingController txtNik = TextEditingController();
  TextEditingController txtNamaAnak = TextEditingController();
  TextEditingController txtTL = TextEditingController();
  TextEditingController txtNT = TextEditingController();
  TextEditingController txtAlamat = TextEditingController();
  TextEditingController txtNikAyah = TextEditingController();
  TextEditingController txtNamaAyah = TextEditingController();
  TextEditingController txtNikIbu = TextEditingController();
  TextEditingController txtNamaIbu = TextEditingController();
  TextEditingController txtAnakKe = TextEditingController();
  TextEditingController txtPBL = TextEditingController();
  TextEditingController txtBBL = TextEditingController();
  TextEditingController txtNoKK = TextEditingController();

  @override
  void initState() {
    super.initState();
    getDesa();
    txtNik.text = widget.anak.nik;
    txtNamaAnak.text = widget.anak.namaAnak;
    txtTL.text = widget.anak.tglLahir;
    txtNT.text = widget.anak.keluarga.noTelp;
    txtAlamat.text = widget.anak.keluarga.alamat;
    txtNikAyah.text = widget.anak.keluarga.nikAyah;
    txtNamaAyah.text = widget.anak.keluarga.namaAyah;
    txtNikIbu.text = widget.anak.keluarga.nikIbu;
    txtNamaIbu.text = widget.anak.keluarga.namaIbu;
    txtAnakKe.text = widget.anak.anakKe;
    txtPBL.text = widget.anak.pbLahir;
    txtBBL.text = widget.anak.bbLahir;
    txtNoKK.text = widget.anak.noKk;
    _gender = widget.anak.jk;
    _kia = widget.anak.kia;
    _imd = widget.anak.imd;
    _idAnak = widget.anak.idAnak;
    _ekonomi = widget.anak.keluarga.statusEkonomi;
    _valDesa = widget.anak.keluarga.idDesa;
    namaDesa = widget.anak.keluarga.desa.namaDesa;
    _getPref();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xff205072),
            ),
            onPressed: () {
              Get.back();
            },
          ),
          title: Text("Form Edit Anak",
              style: TextStyle(color: Color(0xff205072))),
          backgroundColor: Color(0xffe0ecde),
        ),
        body: Form(
          key: _formEditAnak,
          child: ListView(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(20),
                padding: EdgeInsets.all(20),
                child: FutureBuilder<List<Anak>>(
                    future: AnakController.getDetailAnak(widget.anak.idAnak),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.topCenter,
                              child: Text(
                                "Data Anak ",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                            ),
                            TextFormField(
                              validator: (String value) {
                                if (value.length == 0) {
                                  return null;
                                } else if (value.length != 16) {
                                  return "NIK harus 16 Digit";
                                }
                                return null;
                              },
                              controller: txtNik,
                              maxLength: 16,
                              maxLines: 1,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                labelText: "NIK Anak",
                                hintText: "Input NIK Anak",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            TextFormField(
                              controller: txtNamaAnak,
                              maxLength: 50,
                              maxLines: 1,
                              validator: RequiredValidator(
                                  errorText: "Nama anak harus diisi!"),
                              keyboardType: TextInputType.name,
                              decoration: InputDecoration(
                                labelText: "Nama Anak",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text("Jenis Kelamin"),
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Radio(
                                  value: "laki-laki",
                                  groupValue: _gender,
                                  onChanged: (value) {
                                    setState(() {
                                      _gender = value;
                                    });
                                  },
                                ),
                                Text("Laki-laki"),
                                Radio(
                                  value: "perempuan",
                                  groupValue: _gender,
                                  onChanged: (value) {
                                    setState(() {
                                      _gender = value;
                                    });
                                  },
                                ),
                                Text("Perempuan"),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                            ),
                            DateTimeField(
                              controller: txtTL,
                              maxLines: 1,
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.calendar_today),
                                labelText: "Tanggal Lahir",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              format: format,
                              onShowPicker: (context, currentValue) {
                                return showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(1900),
                                  lastDate: DateTime.now(),
                                );
                              },
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                            ),
                            TextFormField(
                              controller: txtAnakKe,
                              maxLength: 13,
                              maxLines: 1,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                labelText: "Anak Ke",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            TextFormField(
                              controller: txtPBL,
                              maxLength: 13,
                              maxLines: 1,
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              decoration: InputDecoration(
                                labelText: "Panjang  Badan Lahir",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            TextFormField(
                              controller: txtBBL,
                              maxLength: 13,
                              maxLines: 1,
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              decoration: InputDecoration(
                                labelText: "Berat Badan Lahir",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text("KIA"),
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Radio(
                                  value: "1",
                                  groupValue: _kia,
                                  onChanged: (value) {
                                    setState(() {
                                      _kia = value;
                                    });
                                  },
                                ),
                                Text("Ya"),
                                Radio(
                                  value: "2",
                                  groupValue: _kia,
                                  onChanged: (value) {
                                    setState(() {
                                      _kia = value;
                                    });
                                  },
                                ),
                                Text("Tidak"),
                              ],
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text("IMD"),
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Radio(
                                  value: "1",
                                  groupValue: _imd,
                                  onChanged: (value) {
                                    setState(() {
                                      _imd = value;
                                    });
                                  },
                                ),
                                Text("Ya"),
                                Radio(
                                  value: "2",
                                  groupValue: _imd,
                                  onChanged: (value) {
                                    setState(() {
                                      _imd = value;
                                    });
                                  },
                                ),
                                Text("Tidak"),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                            ),
                            Align(
                              alignment: Alignment.topCenter,
                              child: Text(
                                "Data Keluarga",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                            ),
                            TextFormField(
                              readOnly: true,
                              enabled: true,
                              controller: txtNoKK,
                              maxLength: 50,
                              maxLines: 1,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                labelText: "No Kartu Keluarga",
                                hintText: "Input No Kartu Keluarga",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            TextFormField(
                              controller: txtNikAyah,
                              maxLength: 50,
                              maxLines: 1,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                labelText: "Nik Ayah",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            TextFormField(
                              controller: txtNamaAyah,
                              maxLength: 50,
                              maxLines: 1,
                              keyboardType: TextInputType.name,
                              decoration: InputDecoration(
                                labelText: "Nama Ayah",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            TextFormField(
                              controller: txtNikIbu,
                              maxLength: 50,
                              maxLines: 1,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                labelText: "Nik Ibu",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            TextFormField(
                              controller: txtNamaIbu,
                              maxLength: 50,
                              maxLines: 1,
                              keyboardType: TextInputType.name,
                              decoration: InputDecoration(
                                labelText: "Nama Ibu",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            TextFormField(
                              controller: txtNT,
                              maxLength: 13,
                              maxLines: 1,
                              keyboardType: TextInputType.phone,
                              decoration: InputDecoration(
                                labelText: "Nomer Telepon",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            TextFormField(
                              controller: txtAlamat,
                              maxLines: null,
                              keyboardType: TextInputType.multiline,
                              decoration: InputDecoration(
                                labelText: "Alamat",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                            ),
                            DropdownSearch<Desa>(
                              mode: Mode.BOTTOM_SHEET,
                              label: "Pilih desa",
                              onFind: (String filter) async {
                                var response = await Dio().get(
                                  ApiService.getDesa(),
                                  queryParameters: {"filter": filter},
                                );
                                var models = Desa.fromJsonList(response.data);
                                return models;
                              },
                              itemAsString: (Desa u) => u.desaAsString(),
                              onChanged: (Desa data) {
                                _valDesa = data.idDesa;
                              },
                              selectedItem: Desa(
                                idDesa: widget.anak.keluarga.idDesa,
                                namaDesa: namaDesa,
                              ),
                              isFilteredOnline: true,
                              showSearchBox: true,
                              searchBoxDecoration: InputDecoration(
                                border: OutlineInputBorder(),
                                contentPadding:
                                    EdgeInsets.fromLTRB(12, 12, 8, 0),
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10)),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text("Status Ekonomi"),
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Radio(
                                  value: "1",
                                  groupValue: _ekonomi,
                                  onChanged: (value) {
                                    setState(() {
                                      _ekonomi = value;
                                    });
                                  },
                                ),
                                Text("1"),
                                Radio(
                                  value: "2",
                                  groupValue: _ekonomi,
                                  onChanged: (value) {
                                    setState(() {
                                      _ekonomi = value;
                                    });
                                  },
                                ),
                                Text("2"),
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  width: 150,
                                  height: 50,
                                  child: RaisedButton(
                                    onPressed: () {
                                      if (_formEditAnak.currentState
                                          .validate()) {
                                        AnakController.updateAnak(
                                          context,
                                          _idAnak,
                                          txtNik.text,
                                          txtNamaAnak.text,
                                          txtTL.text,
                                          _gender,
                                          _kia,
                                          _imd,
                                          txtAnakKe.text,
                                          txtPBL.text,
                                          txtBBL.text,
                                          txtNoKK.text,
                                          txtNikAyah.text,
                                          txtNikIbu.text,
                                          txtNamaAyah.text,
                                          txtNamaIbu.text,
                                          txtNT.text,
                                          _valDesa,
                                          _posyandu,
                                          _ekonomi,
                                          txtAlamat.text,
                                        );
                                      }
                                    },
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(80.0),
                                    ),
                                    padding: EdgeInsets.all(5),
                                    color: Color(0xff205072),
                                    elevation: 5,
                                    child: Text(
                                      "Ubah Data",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            //SubmitButton(Style.buttonStyle),
                          ],
                        );
                      } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                      }
                      return SpinKitCubeGrid(
                        color: Colors.blue,
                        size: 30.0,
                        controller: AnimationController(
                          vsync: this,
                          duration: const Duration(milliseconds: 1900),
                        ),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
