import 'dart:async';

import 'package:flutter/material.dart';
import 'package:division/division.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:indonesia/indonesia.dart';
import 'package:inpos/models/anak.dart';
import 'package:shimmer/shimmer.dart';
import 'package:auto_size_text/auto_size_text.dart';

class CardDetailAnak extends StatefulWidget {
  final ParentStyle cardDetailStyle;
  final Anak anak;
  CardDetailAnak(this.anak, this.cardDetailStyle);

  @override
  CardDetailAnakState createState() => CardDetailAnakState();
}

class CardDetailAnakState extends State<CardDetailAnak>
    with TickerProviderStateMixin {
  bool _enable = false;
  Timer _timer;

  @override
  void initState() {
    super.initState();
    _timer = Timer(Duration(seconds: 1), () {
      setState(() {
        _enable = true;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Parent(
      child: FutureBuilder<List<Anak>>(
        future: AnakController.getDetailAnak(widget.anak.idAnak),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Anak> data = snapshot.data;
            return _enable
                ? _anak(data)
                : Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[100],
                    child: _anak(data),
                  );
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          return SpinKitCubeGrid(
            color: Colors.blue,
            size: 30.0,
            controller: AnimationController(
              vsync: this,
              duration: const Duration(seconds: 3),
            ),
          );
        },
      ),
    );
  }

  ListView _anak(data) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: data.length,
        itemBuilder: (context, index) {
          return Parent(
            style: widget.cardDetailStyle,
            child: Column(
              children: [
                _tile(
                  data[index].namaAnak,
                  data[index].nik,
                  data[index].tglLahir,
                  data[index].jk,
                  data[index].pbLahir,
                  data[index].bbLahir,
                  data[index].anakKe,
                  data[index].keluarga.namaAyah,
                  data[index].keluarga.namaIbu,
                  data[index].kia,
                  data[index].imd,
                  data[index].noKk,
                  data[index].keluarga.statusEkonomi,
                  data[index].keluarga.alamat,
                ),
              ],
            ),
          );
        });
  }

  ListTile _tile(
    String nama,
    String nik,
    String tglLahir,
    String jk,
    String pbLahir,
    String bbLahir,
    String anakKe,
    String namaAyah,
    String namaIbu,
    String kia,
    String imd,
    String noKk,
    String statusEkonomi,
    String alamat,
  ) =>
      ListTile(
        title: Container(
          margin: EdgeInsets.only(top: 15, bottom: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                title: Text(
                  "No KK ",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 15),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    '$noKk',
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "NIK ",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 15),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    nik != null ? "$nik" : "-",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Nama ",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 15),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    "$nama",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Tgl Lahir",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    tanggal(DateTime.parse(tglLahir)),
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 14),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Jenis Kelamin",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    "$jk",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Panjang Badan Lahir",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    "$pbLahir Cm",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Berat Badan Lahir",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    "$bbLahir Kg",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Anak ke-",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    "$anakKe",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Orangtua",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    " $namaAyah - $namaIbu",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 13),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "KIA",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    kia == '1' ? "Ya" : "Tidak",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 13),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "IMD",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    imd == '1' ? "Ya" : "Tidak",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 13),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Alamat",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    "$alamat",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 13),
                  ),
                ),
              ),
              Divider(
                color: Color(0xff205072),
                thickness: 2.0,
                height: 10,
                indent: 10,
                endIndent: 15,
              ),
              ListTile(
                title: Text(
                  "Status Ekonomi",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 13),
                ),
                trailing: Container(
                  child: AutoSizeText(
                    "$statusEkonomi",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 13),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
