import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:inpos/models/anak.dart';
import 'package:inpos/models/desa.dart';
import 'package:inpos/services/api_service.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormAddAnak extends StatefulWidget {
  @override
  _FormAddAnakState createState() => _FormAddAnakState();
}

class _FormAddAnakState extends State<FormAddAnak> {
  int _valDesa, _posyandu;
  String _gender, _kia, _imd, _ekonomi;
  final format = DateFormat("yyyy-MM-dd");
  final _formAddAnak = GlobalKey<FormState>();
  List<dynamic> dataDesa = List.empty();

  void getDesa() async {
    final response = await http.get(ApiService.getDesa());
    if (response.body.isNotEmpty) {
      setState(() {
        dataDesa = jsonDecode(response.body);
      });
    }
  }

  _getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      _posyandu = pref.getInt('id_posyandu');
    });
  }

  @override
  void initState() {
    super.initState();
    getDesa();
    _getPref();
  }

  TextEditingController txtNik = TextEditingController();
  TextEditingController txtNamaAnak = TextEditingController();
  TextEditingController txtTL = TextEditingController();
  TextEditingController txtNT = TextEditingController();
  TextEditingController txtAlamat = TextEditingController();
  TextEditingController txtNikAyah = TextEditingController();
  TextEditingController txtNamaAyah = TextEditingController();
  TextEditingController txtNikIbu = TextEditingController();
  TextEditingController txtNamaIbu = TextEditingController();
  TextEditingController txtAnakKe = TextEditingController();
  TextEditingController txtPBL = TextEditingController();
  TextEditingController txtBBL = TextEditingController();
  TextEditingController txtNoKK = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xff205072),
            ),
            onPressed: () {
              Get.back();
            },
          ),
          title: Text("Form Tambah Anak",
              style: TextStyle(color: Color(0xff205072))),
          backgroundColor: Color(0xffe0ecde),
        ),
        body: Form(
          key: _formAddAnak,
          child: ListView(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(20),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topCenter,
                      child: Text(
                        "Data Anak",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                    ),
                    TextFormField(
                      controller: txtNik,
                      keyboardType: TextInputType.number,
                      maxLength: 16,
                      maxLines: 1,
                      validator: (String value) {
                        if (value.length == 0) {
                          return null;
                        } else if (value.length != 16) {
                          return "NIK harus 16 Digit";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        labelText: "NIK Anak",
                        hintText: "Input NIK Anak",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    TextFormField(
                      controller: txtNamaAnak,
                      maxLength: 50,
                      maxLines: 1,
                      validator: RequiredValidator(
                        errorText: "Nama anak tidak boleh kosong",
                      ),
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                        labelText: "Nama Anak",
                        hintText: "Input Nama Anak",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text("Jenis Kelamin"),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: FormBuilderRadioGroup(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                            ),
                            validators: [
                              FormBuilderValidators.required(
                                errorText: "Jenis kelamin harus diisi!",
                              )
                            ],
                            attribute: "Jenis Kelamin",
                            options: ["laki-laki", "perempuan"]
                                .map((jk) => FormBuilderFieldOption(
                                      value: jk,
                                      child: Text(jk.capitalizeFirst),
                                    ))
                                .toList(),
                            onChanged: (value) {
                              setState(() {
                                _gender = value;
                                print(_gender);
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                    ),
                    DateTimeField(
                      controller: txtTL,
                      maxLines: 1,
                      validator: (date) => date == null
                          ? "Tanggal lahir tidak boleh kosong!"
                          : null,
                      decoration: InputDecoration(
                        suffixIcon: Icon(Icons.calendar_today),
                        labelText: "Tanggal Lahir",
                        hintText: "Input tanggal lahir Anak",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      format: format,
                      onShowPicker: (context, currentValue) {
                        return showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2010),
                          lastDate: DateTime.now(),
                        );
                      },
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    ),
                    TextFormField(
                      controller: txtAnakKe,
                      maxLength: 13,
                      maxLines: 1,
                      validator: RequiredValidator(
                        errorText: "Anak ke tidak boleh kosong!",
                      ),
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: "Input Anak Ke ...",
                        labelText: "Anak Ke",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    TextFormField(
                      controller: txtPBL,
                      maxLength: 13,
                      maxLines: 1,
                      validator: RequiredValidator(
                        errorText: "Panjang badan tidak boleh kosong!",
                      ),
                      keyboardType: TextInputType.numberWithOptions(
                        decimal: true,
                      ),
                      decoration: InputDecoration(
                        hintText: "Input Panjang  Badan",
                        labelText: "Panjang  Badan Lahir",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    TextFormField(
                      controller: txtBBL,
                      maxLength: 13,
                      maxLines: 1,
                      validator: RequiredValidator(
                        errorText: "Berat badan tidak boleh kosong!",
                      ),
                      keyboardType: TextInputType.numberWithOptions(
                        decimal: true,
                      ),
                      decoration: InputDecoration(
                        hintText: "Input Berat Badan",
                        labelText: "Berat Badan Lahir",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text("KIA"),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: FormBuilderRadioGroup(
                            decoration:
                                InputDecoration(border: InputBorder.none),
                            validators: [
                              FormBuilderValidators.required(
                                  errorText: "KIA harus diisi!")
                            ],
                            attribute: "KIA",
                            options: ["1", "2"]
                                .map((kia) => FormBuilderFieldOption(
                                      value: kia,
                                      child: Text("$kia"),
                                    ))
                                .toList(),
                            onChanged: (value) {
                              setState(() {
                                _kia = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text("IMD"),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: FormBuilderRadioGroup(
                            decoration:
                                InputDecoration(border: InputBorder.none),
                            validators: [
                              FormBuilderValidators.required(
                                  errorText: "IMD harus diisi!")
                            ],
                            attribute: "IMD",
                            options: ["1", "2"]
                                .map((imd) => FormBuilderFieldOption(
                                      value: imd,
                                      child: Text("$imd"),
                                    ))
                                .toList(),
                            onChanged: (value) {
                              setState(() {
                                _imd = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Text(
                        "Data Keluarga",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    ),
                    TextFormField(
                      controller: txtNoKK,
                      maxLength: 16,
                      maxLines: 1,
                      validator: MultiValidator([
                        RequiredValidator(
                            errorText: "NO KK Tidak Boleh Kosong"),
                        MinLengthValidator(
                          16,
                          errorText: "NO KK Harus 16 Digit",
                        ),
                      ]),
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: "No Kartu Keluarga",
                        hintText: "Input No Kartu Keluarga",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    TextFormField(
                      controller: txtNikAyah,
                      maxLength: 16,
                      maxLines: 1,
                      validator: MultiValidator([
                        RequiredValidator(
                            errorText: "Nik Ayah Tidak Boleh Kosong"),
                        MinLengthValidator(16, errorText: "NIK Harus 16 Digit"),
                      ]),
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: "Nik Ayah",
                        hintText: "Input Nik Ayah",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    TextFormField(
                      controller: txtNamaAyah,
                      maxLength: 50,
                      maxLines: 1,
                      validator: RequiredValidator(
                        errorText: "Nama Ayah Tidak Boleh Kosong",
                      ),
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                        labelText: "Nama Ayah",
                        hintText: "Input Nama Ayah",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    TextFormField(
                      controller: txtNikIbu,
                      maxLength: 16,
                      maxLines: 1,
                      validator: MultiValidator([
                        RequiredValidator(
                            errorText: "Nik Ibu Tidak Boleh Kosong"),
                        MinLengthValidator(16, errorText: "NIK Harus 16 Digit")
                      ]),
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: "Nik Ibu",
                        hintText: "Input Nik Ibu",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    TextFormField(
                      controller: txtNamaIbu,
                      maxLength: 50,
                      maxLines: 1,
                      validator: RequiredValidator(
                        errorText: "Nama Ibu Tidak Boleh Kosong",
                      ),
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                        labelText: "Nama Ibu",
                        hintText: "Input Nama Ibu",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    TextFormField(
                      controller: txtNT,
                      maxLength: 13,
                      maxLines: 1,
                      validator: RequiredValidator(
                        errorText: "No Telepon Tidak Boleh Kosong",
                      ),
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        hintText: "Input No Telepon",
                        labelText: "Nomer Telepon",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    TextFormField(
                      controller: txtAlamat,
                      maxLines: null,
                      validator: RequiredValidator(
                        errorText: "Alamat Tidak Boleh Kosong",
                      ),
                      keyboardType: TextInputType.multiline,
                      decoration: InputDecoration(
                        labelText: "Alamat",
                        hintText: "Input Alamat",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    Container(margin: EdgeInsets.fromLTRB(0, 10, 0, 10)),
                    DropdownSearch<Desa>(
                      mode: Mode.BOTTOM_SHEET,
                      hint: "Nama desa",
                      label: "Pilih desa",
                      onFind: (String filter) async {
                        var response = await Dio().get(
                          ApiService.getDesa(),
                          queryParameters: {"filter": filter},
                        );
                        var models = Desa.fromJsonList(response.data);
                        return models;
                      },
                      validator: (Desa v) =>
                          v == null ? "Data desa tidak boleh kosong" : null,
                      autoValidateMode: AutovalidateMode.onUserInteraction,
                      itemAsString: (Desa u) => u.desaAsString(),
                      onChanged: (Desa data) {
                        _valDesa = data.idDesa;
                        print(data);
                      },
                      isFilteredOnline: true,
                      showSearchBox: true,
                      searchBoxDecoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                      ),
                    ),
                    Container(margin: EdgeInsets.fromLTRB(0, 10, 0, 10)),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text("Status Ekonomi"),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: FormBuilderRadioGroup(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                            ),
                            validators: [
                              FormBuilderValidators.required(
                                errorText: "Status Ekonomi harus diisi!",
                              )
                            ],
                            attribute: "ekonomi",
                            options: ["1", "2"]
                                .map((ekonomi) => FormBuilderFieldOption(
                                      value: ekonomi,
                                      child: Text("$ekonomi"),
                                    ))
                                .toList(),
                            onChanged: (value) {
                              setState(() {
                                _ekonomi = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          width: 150,
                          height: 50,
                          child: RaisedButton(
                            onPressed: () {
                              if (_formAddAnak.currentState.validate()) {
                                AnakController.createAnak(
                                  context,
                                  txtNik.text,
                                  txtNamaAnak.text,
                                  txtTL.text,
                                  _gender,
                                  _kia,
                                  _imd,
                                  txtAnakKe.text,
                                  txtPBL.text,
                                  txtBBL.text,
                                  txtNoKK.text,
                                  txtNikAyah.text,
                                  txtNikIbu.text,
                                  txtNamaAyah.text,
                                  txtNamaIbu.text,
                                  txtNT.text,
                                  _valDesa,
                                  _posyandu,
                                  _ekonomi,
                                  txtAlamat.text,
                                );
                              }
                              print(_gender);
                              print(_kia);
                              print(_imd);
                              print(_valDesa);
                              print(_posyandu);
                              print(_ekonomi);
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(80.0),
                            ),
                            padding: EdgeInsets.all(5),
                            color: Color(0xff205072),
                            elevation: 5,
                            child: Text(
                              "Simpan Data",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
