import 'package:flutter/material.dart';
import 'package:inpos/views/styles/style.dart';
import 'package:inpos/views/pages/home_page.dart';
import 'package:inpos/views/pages/add_button.dart';
import 'package:inpos/views/pages/anak/anak_page.dart';
import 'package:inpos/views/pages/gizi/gizi_page.dart';
import 'package:inpos/views/pages/profile/profile.dart';
import 'package:inpos/views/pages/imunisasi/imunisasi_page.dart';

class MainPage extends StatefulWidget {
  final int indexCurrent;
  MainPage({this.indexCurrent});
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _selectedIndex;
  int _pIndex = 0;

  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.indexCurrent ?? 0;
  }

  Future<bool> _onWilPop() {
    setState(() {
      _selectedIndex = _pIndex;
    });
    return null;
  }

  TextEditingController searchController = TextEditingController();

  final tabs = [
    HomePage(),
    SizedBox(child: AnakPage(Style.cardStyle)),
    ImunisasiPage(Style.cardStyle),
    GiziPage(Style.cardStyle),
    ProfilePage()
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWilPop,
      child: Scaffold(
        body: tabs[_selectedIndex],
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.home), label: "Beranda"),
            BottomNavigationBarItem(
              icon: Icon(Icons.child_care_outlined),
              label: "Anak",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.colorize),
              label: "Imunisasi",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.restaurant),
              label: "Gizi",
            ),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profile"),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Color(0xff205072),
          unselectedItemColor: Color(0xffe0ecde),
          showUnselectedLabels: true,
          onTap: (index) {
            setState(() {
              _selectedIndex = index;
            });
          },
        ),
        floatingActionButton: _addButton(_selectedIndex),
      ),
    );
  }

  Widget _addButton(int index) {
    return index == 0 || index == 4 ? null : AddButton(index: index);
  }
}
