import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:inpos/controller/user_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:form_field_validator/form_field_validator.dart';

class FormEditProfile extends StatefulWidget {
  @override
  _FormEditProfileState createState() => _FormEditProfileState();
}

class _FormEditProfileState extends State<FormEditProfile> {
  String username, name, jk, alamat;
  int id;
  //String _posyandu;

  TextEditingController txtUsername = TextEditingController();
  TextEditingController txtName = TextEditingController();
  TextEditingController txtAlamat = TextEditingController();

  getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      id = pref.getInt('id');
      txtUsername.text = pref.getString('username');
      txtName.text = pref.getString('name');
      //_posyandu = pref.getString('nama_posyandu');
      jk = pref.getString('jk');
      txtAlamat.text = pref.getString('alamat');
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xff205072),
            ),
            onPressed: () {
              Get.back();
            },
          ),
          title: Text(
            "Form Edit Profile",
            style: TextStyle(color: Color(0xff205072)),
          ),
          backgroundColor: Color(0xffe0ecde),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  TextFormField(
                    controller: txtUsername,
                    validator:
                        RequiredValidator(errorText: "Username harus diisi!"),
                    maxLength: 50,
                    keyboardType: TextInputType.name,
                    decoration: InputDecoration(
                      labelText: "Username",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  TextFormField(
                    controller: txtName,
                    maxLength: 50,
                    maxLines: 1,
                    keyboardType: TextInputType.name,
                    decoration: InputDecoration(
                      labelText: "Nama",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text("Jenis Kelamin"),
                  ),
                  ButtonBar(
                    alignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Radio(
                        value: "laki-laki",
                        groupValue: jk,
                        onChanged: (value) {
                          setState(() {
                            jk = value;
                          });
                        },
                      ),
                      Text("Laki-laki"),
                      Radio(
                        value: "perempuan",
                        groupValue: jk,
                        onChanged: (value) {
                          setState(() {
                            jk = value;
                          });
                        },
                      ),
                      Text("Perempuan"),
                    ],
                  ),
                  TextFormField(
                    controller: txtAlamat,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                      labelText: "Alamat",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 150,
                        height: 50,
                        child: RaisedButton(
                          color: Color(0xff205072),
                          onPressed: () {
                            UserController.updateProfile(
                              context,
                              id,
                              txtUsername.text,
                              txtName.text,
                              jk,
                              txtAlamat.text,
                            );
                          },
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0),
                          ),
                          padding: EdgeInsets.all(5),
                          elevation: 5,
                          child: Text(
                            "Simpan Data",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
