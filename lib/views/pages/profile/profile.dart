import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:inpos/controller/user_controller.dart';
import 'package:inpos/views/styles/style.dart';
import 'package:inpos/views/pages/profile/card_profile.dart';
import 'package:inpos/views/pages/profile/form_edit_profile.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          CardProfile(Style.cardStyle),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 20),
                width: 150,
                height: 50,
                child: RaisedButton(
                  onPressed: () {
                    Get.to(
                      FormEditProfile(),
                      transition: Transition.zoom,
                      duration: Duration(seconds: 2),
                    );
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80.0),
                  ),
                  padding: EdgeInsets.all(5),
                  color: Color(0xff205072),
                  elevation: 5,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.edit, color: Colors.white),
                      Text(
                        " Ubah Profile",
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 20),
                width: 150,
                height: 50,
                child: RaisedButton(
                  onPressed: () {
                    UserController.logout();
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80.0),
                  ),
                  padding: EdgeInsets.all(5),
                  color: Colors.red,
                  elevation: 5,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.logout, color: Colors.white),
                      Text(
                        " Logout",
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
