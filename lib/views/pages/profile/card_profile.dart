import 'package:flutter/material.dart';
import 'package:division/division.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CardProfile extends StatefulWidget {
  final ParentStyle cardStyle;
  CardProfile(this.cardStyle);

  @override
  CardProfileState createState() => CardProfileState();
}

class CardProfileState extends State<CardProfile> {
  String _username, _name, _jk, _alamat, _posyandu;

  _getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      _username = pref.getString('username');
      _name = pref.getString('name');
      _posyandu = pref.getString('nama_posyandu');
      _jk = pref.getString('jk');
      _alamat = pref.getString('alamat');
    });
  }

  @override
  void initState() {
    super.initState();
    _getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Parent(
      child: Stack(
        children: <Widget>[
          SizedBox(
            width: double.infinity,
            height: 160.0,
            child: Material(
              color: Color(0xffe0ecde),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(34),
                bottomRight: Radius.circular(34),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 80.0),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: CircleAvatar(
                radius: 55.0,
                backgroundColor: Colors.white,
                child: CircleAvatar(
                  backgroundColor: Color(0xff205072),
                  radius: 50,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Text(_name != null ? _name[0] : "",
                        style: TextStyle(fontSize: 60, color: Colors.white),
                        textAlign: TextAlign.center),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 200, 10, 40),
            child: Card(
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(14),
              ),
              child: Column(
                children: [
                  ListTile(
                    title: Text(
                      'Username : $_username',
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                  ),
                  Divider(
                    color: Color(0xff205072),
                    thickness: 2.0,
                    height: 10,
                    indent: 20,
                    endIndent: 30,
                  ),
                  ListTile(
                    title: Text(
                      'Nama : $_name',
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                  ),
                  Divider(
                    color: Color(0xff205072),
                    thickness: 2.0,
                    height: 10,
                    indent: 20,
                    endIndent: 30,
                  ),
                  ListTile(
                    title: Text(
                      'Jenis Kelamin : $_jk',
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                  ),
                  Divider(
                    color: Color(0xff205072),
                    thickness: 2.0,
                    height: 10,
                    indent: 20,
                    endIndent: 30,
                  ),
                  ListTile(
                    title: Text(
                      'Posyandu : $_posyandu',
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                  ),
                  Divider(
                    color: Color(0xff205072),
                    thickness: 2.0,
                    height: 10,
                    indent: 20,
                    endIndent: 30,
                  ),
                  ListTile(
                    title: Text(
                      'Alamat : $_alamat',
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
