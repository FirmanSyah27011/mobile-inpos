import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:inpos/views/pages/anak/form_add_anak.dart';
import 'package:inpos/views/pages/gizi/form_add_gizi.dart';
import 'package:inpos/views/pages/imunisasi/form_add_imunisasi.dart';

class AddButton extends StatelessWidget {
  final int index;
  const AddButton({Key key, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          if (index == 1) {
            Get.to(
              FormAddAnak(),
              transition: Transition.rightToLeftWithFade,
              duration: Duration(seconds: 1),
            );
          } else if (index == 2) {
            Get.to(
              FormAddImunisasi(),
              transition: Transition.topLevel,
              duration: Duration(seconds: 1),
            );
          } else if (index == 3) {
            Get.to(
              FormAddGizi(),
              transition: Transition.cupertino,
              duration: Duration(seconds: 1),
            );
          }
        },
      ),
    );
  }
}
