import 'package:division/division.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

abstract class Style {
  bool pressed = false;

  static ParentStyle buttonStyle = ParentStyle()
    ..margin(all: 20)
    ..background.color(Colors.blue)
    ..borderRadius(all: 80)
    ..border(all: 1, color: Colors.black)
    ..elevation(5)
    ..padding(vertical: 20, horizontal: 40)
    ..ripple(true, splashColor: Colors.white);

  static TxtStyle txtStyle = TxtStyle()
    ..fontSize(20)
    ..bold()
    ..textColor(Colors.white);

  static ParentStyle cardStyle = ParentStyle()
    ..borderRadius(all: 14)
    ..margin(top: 15, bottom: 5, left: 10, right: 10)
    ..padding(vertical: 0, horizontal: 5)
    ..background.color(Colors.white)
    ..elevation(5);

  static ParentStyle cardDetailStyle = ParentStyle()
    ..borderRadius(all: 14)
    ..margin(all: 20)
    ..padding(vertical: 10, horizontal: 10)
    ..background.color(Colors.white)
    ..elevation(5);

  static AlertStyle alertSuccess = AlertStyle(
      animationType: AnimationType.fromTop,
      isCloseButton: false,
      isButtonVisible: false,
      isOverlayTapDismiss: false,
      animationDuration: Duration(milliseconds: 400),
      titleStyle: TextStyle(color: Colors.black));

  static AlertStyle alertStyle = AlertStyle(
      animationType: AnimationType.fromTop,
      isCloseButton: false,
      isOverlayTapDismiss: false,
      animationDuration: Duration(milliseconds: 400),
      titleStyle: TextStyle(color: Colors.black));
}
