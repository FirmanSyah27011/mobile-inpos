import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static savePref(
    int id,
    String username,
    String name,
    String jk,
    String alamat,
    int idPosyandu,
    String namaPosyandu,
    String namaPuskesmas,
    String alamatPuskesmas,
  ) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt("id", id);
    preferences.setString("username", username);
    preferences.setString("name", name);
    preferences.setString('jk', jk);
    preferences.setString('alamat', alamat);
    preferences.setInt("id_posyandu", idPosyandu);
    preferences.setString("nama_posyandu", namaPosyandu);
    preferences.setString("nama_puskesmas", namaPuskesmas);
    preferences.setString("alamat_puskesmas", alamatPuskesmas);
  }

  static updatePref(
    String username,
    String name,
    String jk,
    String alamat,
  ) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("username", username);
    preferences.setString("name", name);
    preferences.setString('jk', jk);
    preferences.setString('alamat', alamat);
  }

  static getPref() async {
    /* SharedPreferences pref = await SharedPreferences.getInstance();
    id = pref.getInt("id");
    username = pref.getString('username');
    name = pref.getString('name');
    idPosyandu = pref.getString('id_posyandu');
    namaPosyandu = pref.getString('nama_posyandu');
    jk = pref.getString('jk');
    alamat = pref.getString('alamat');
    namaPuskesmas = pref.getString("nama_puskesmas");
    alamatPuskesmas = pref.getString("alamat_puskesmas"); */
  }

  static destroyPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.remove('username');
    pref.remove('name');
    pref.remove('jk');
    pref.remove('alamat');
    pref.remove('id_posyandu');
    pref.remove('nama_posyandu');
  }
}
