class StatusGizi {
  final int idStatus;
  final String bbu;
  final String pbtbu;
  final String bbpbtb;

  StatusGizi({
    this.idStatus,
    this.bbu,
    this.pbtbu,
    this.bbpbtb,
  });

  factory StatusGizi.fromJson(Map<String, dynamic> obj) {
    if (obj == null) return null;
    return StatusGizi(
      idStatus: obj['id_status'] as int,
      bbu: obj['bb_u'] as String,
      pbtbu: obj['pb_tb_u'] as String,
      bbpbtb: obj['bb_pb_tb'] as String,
    );
  }

  static List<StatusGizi> fromJsonList(List list) {
    if (list == null) return null;
    return list.map((item) => StatusGizi.fromJson(item)).toList();
  }
}
