class Desa {
  final int idDesa;
  final String namaDesa;

  Desa({this.idDesa, this.namaDesa});

  factory Desa.fromJson(Map<String, dynamic> obj) {
    if (obj == null) return null;
    return Desa(
      idDesa: obj['id_desa'] as int,
      namaDesa: obj['nama_desa'] as String,
    );
  }

  static List<Desa> fromJsonList(List list) {
    if (list == null) return null;
    return list.map((item) => Desa.fromJson(item)).toList();
  }

  String desaAsStringById() => '${this.idDesa} ${this.namaDesa}';

  String desaAsString() => '${this.namaDesa}';

  bool isEqual(Desa model) => this?.idDesa == model?.idDesa;

  @override
  String toString() => namaDesa;
}
