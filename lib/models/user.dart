class User {
  int id;
  String username;
  String name;
  String jk;
  String alamat;
  int posyandu;
  String namaPosyandu;
  String namaPuskesmas;
  String alamatPuskesmas;

  User({
    this.id,
    this.username,
    this.name,
    this.jk,
    this.alamat,
    this.posyandu,
    this.namaPosyandu,
    this.namaPuskesmas,
    this.alamatPuskesmas,
  });

  factory User.fromJson(Map<String, dynamic> obj) {
    if (obj == null) return null;
    return User(
      id: obj['id_user'] as int,
      username: obj['username'] as String,
      name: obj['name'] as String,
      jk: obj['jk'] as String,
      alamat: obj['alamat'] as String,
      posyandu: obj['posyandu']['id_posyandu'] as int,
      namaPosyandu: obj['posyandu']['nama_posyandu'] as String,
      namaPuskesmas: obj['posyandu']['puskesmas']['nama_puskesmas'] as String,
      alamatPuskesmas: obj['posyandu']['puskesmas']['alamat'] as String,
    );
  }
}
