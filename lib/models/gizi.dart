import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:inpos/models/anak.dart';
import 'package:inpos/models/status_gizi.dart';
import 'package:http/http.dart' as http;
import 'package:inpos/services/api_service.dart';
import 'package:inpos/views/styles/style.dart';
import 'package:inpos/views/pages/main_page.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'dart:async';
import 'package:intl/intl.dart';
part "package:inpos/controller/gizi_controller.dart";

class Gizi {
  final String noPemeriksaanGizi;
  final String usia;
  final int pbTb;
  final double bb;
  final String tglPeriksa;
  final String caraUkur;
  final String asiEks;
  final String vitA;
  final String validasi;
  final int idStatusGizi;
  final int idAnak;
  final Anak anak;
  final StatusGizi statusGizi;

  Gizi({
    this.noPemeriksaanGizi,
    this.usia,
    this.pbTb,
    this.bb,
    this.tglPeriksa,
    this.caraUkur,
    this.asiEks,
    this.vitA,
    this.validasi,
    this.idStatusGizi,
    this.idAnak,
    this.anak,
    this.statusGizi,
  });

  factory Gizi.fromJson(Map<String, dynamic> obj) {
    if (obj == null) return null;
    return Gizi(
      noPemeriksaanGizi: obj['no_pemeriksaan_gizi'] as String,
      usia: obj['usia'] as String,
      pbTb: obj['pb_tb'] as int,
      bb: obj['bb'].toDouble(),
      tglPeriksa: obj['tgl_periksa'] as String,
      caraUkur: obj['cara_ukur'] as String,
      asiEks: obj['asi_eks'] as String,
      vitA: obj['vit_a'] as String,
      validasi: obj['validasi'] as String,
      idStatusGizi: obj['id_status_gizi'] as int,
      idAnak: obj['id_anak'] as int,
      anak: Anak.fromJson(obj['anak']),
      statusGizi: StatusGizi.fromJson(obj['status_gizi']),
    );
  }
}
