//import 'dart:convert';

import 'package:inpos/models/desa.dart';

class Keluarga {
  String noKk;
  String nikAyah;
  String nikIbu;
  String namaAyah;
  String namaIbu;
  String noTelp;
  String statusEkonomi;
  String alamat;
  int idDesa;
  Desa desa;

  Keluarga({
    this.noKk,
    this.nikAyah,
    this.nikIbu,
    this.namaAyah,
    this.namaIbu,
    this.noTelp,
    this.statusEkonomi,
    this.alamat,
    this.idDesa,
    this.desa,
  });

  factory Keluarga.fromJson(Map<String, dynamic> obj) {
    if (obj == null) return null;
    return Keluarga(
      noKk: obj['no_kk'] as String,
      nikAyah: obj['nik_ayah'] as String,
      nikIbu: obj['nik_ibu'] as String,
      namaAyah: obj['nama_ayah'] as String,
      namaIbu: obj['nama_ibu'] as String,
      noTelp: obj['no_telp'] as String,
      statusEkonomi: obj['status_ekonomi'] as String,
      alamat: obj['alamat'] as String,
      idDesa: obj['id_desa'] as int,
      desa: Desa.fromJson(obj['desa']),
    );
  }

  static List<Keluarga> fromJsonList(List list) {
    if (list == null) return null;
    return list.map((item) => Keluarga.fromJson(item)).toList();
  }
}
