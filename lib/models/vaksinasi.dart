class Vaksinasi {
  final int idVaksinasi;
  final String namaVaksinasi;

  Vaksinasi({this.idVaksinasi, this.namaVaksinasi});

  factory Vaksinasi.fromJson(Map<String, dynamic> obj) {
    if (obj == null) return null;
    return Vaksinasi(
      idVaksinasi: obj['id_vaksinasi'] as int,
      namaVaksinasi: obj['nama_vaksinasi'] as String,
    );
  }

  static List<Vaksinasi> fromJsonList(List list) {
    if (list == null) return null;
    return list.map((item) => Vaksinasi.fromJson(item)).toList();
  }

  String vaksinasiAsString() => '${this.namaVaksinasi}';

  @override
  String toString() => namaVaksinasi;
}
