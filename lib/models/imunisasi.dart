import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:inpos/services/api_service.dart';
import 'package:inpos/models/anak.dart';
import 'package:inpos/models/vaksinasi.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:inpos/views/styles/style.dart';
import 'package:inpos/views/pages/main_page.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'dart:async';
part "package:inpos/controller/imunisasi_controller.dart";

class Imunisasi {
  final String noPemeriksaanImunisasi;
  final String tglImunisasi;
  final int idVaksinasi;
  final int idAnak;
  final Anak anak;
  final Vaksinasi vaksinasi;

  Imunisasi({
    this.noPemeriksaanImunisasi,
    this.tglImunisasi,
    this.idVaksinasi,
    this.idAnak,
    this.anak,
    this.vaksinasi,
  });

  factory Imunisasi.fromJson(Map<String, dynamic> obj) {
    if (obj == null) return null;
    return Imunisasi(
      noPemeriksaanImunisasi: obj['no_pemeriksaan_imunisasi'] as String,
      tglImunisasi: obj['tgl_imunisasi'] as String,
      idVaksinasi: obj['id_vaksinasi'] as int,
      idAnak: obj['id_anak'] as int,
      anak: Anak.fromJson(obj['anak']),
      vaksinasi: Vaksinasi.fromJson(obj['vaksinasi']),
    );
  }
}
