import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:inpos/models/keluarga.dart';
import 'package:http/http.dart' as http;
import 'package:inpos/services/api_service.dart';
import 'package:inpos/views/styles/style.dart';
import 'package:inpos/views/pages/main_page.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'dart:async';
part "package:inpos/controller/anak_controller.dart";

class Anak {
  final int idAnak;
  final String nik;
  final String namaAnak;
  final String tglLahir;
  final String jk;
  final String anakKe;
  final String bbLahir;
  final String pbLahir;
  final String kia;
  final String imd;
  final String noKk;
  final Keluarga keluarga;
  final int idPosyandu;

  Anak({
    this.idAnak,
    this.nik,
    this.namaAnak,
    this.tglLahir,
    this.jk,
    this.anakKe,
    this.bbLahir,
    this.pbLahir,
    this.kia,
    this.imd,
    this.keluarga,
    this.noKk,
    this.idPosyandu,
  });

  factory Anak.fromJson(Map<String, dynamic> obj) {
    if (obj == null) return null;
    return Anak(
      idAnak: obj['id_anak'] as int,
      nik: obj['nik'] as String,
      namaAnak: obj['nama_anak'] as String,
      tglLahir: obj['tgl_lahir'] as String,
      jk: obj['jk'] as String,
      anakKe: obj['anak_ke'] as String,
      bbLahir: obj['bb_lahir'] as String,
      pbLahir: obj['pb_lahir'] as String,
      kia: obj['kia'] as String,
      imd: obj['imd'] as String,
      noKk: obj['no_kk'] as String,
      idPosyandu: obj['id_posyandu'] as int,
      keluarga: Keluarga.fromJson(obj['keluarga']),
    );
  }

  static List<Anak> fromJsonList(List list) {
    if (list == null) return null;
    return list.map((item) => Anak.fromJson(item)).toList();
  }

  String anakAsString() => '${this.namaAnak} ${this.nik != null ? nik : "-"} ';

  @override
  String toString() => namaAnak;
}
