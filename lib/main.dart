import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:get/get.dart';
import 'package:inpos/views/pages/login.dart';
import 'package:inpos/views/pages/main_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  FlutterStatusbarcolor.setStatusBarColor(Color(0xffe0ecde));
  FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var username = prefs.getString('username');
  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: username == null ? Login() : MainPage(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: Login(),
    );
  }
}
