class ApiService {
  //other host http://192.168.43.212:8080/api
  //static String baseUrl = "http://192.168.43.211:8080/api";

  static String baseUrl = "http://1805049.web.ti.polindra.ac.id/api";
  //static String baseUrl = "http://192.168.100.60:8080/api";
  //Posyandu dan anak int, id gizi dan imunisasi String

  //anak
  static String getAnak(int id) => "$baseUrl/getAnakByPosyandu/$id";
  static String getDetailAnak(int id) => "$baseUrl/detail-anak/$id";
  static String postAnak() => "$baseUrl/anak";
  static String updateAnak(int id) => "$baseUrl/edit-anak/$id";

  //Gizi
  static String getGizi(int id) => "$baseUrl/getGiziByPosyandu/$id";
  static String getDetailGizi(String id) => "$baseUrl/detail-gizi/$id";
  static String postGizi() => "$baseUrl/add-gizi";
  static String updateGizi(String id) => "$baseUrl/edit-gizi/$id";

  //imunisasi
  static String getImunisasi(int id) => "$baseUrl/getImunisasiByPosyandu/$id";
  static String getDetailImun(String id) => "$baseUrl/detail-imunisasi/$id";
  static String postImunisasi() => "$baseUrl/add-imunisasi";
  static String updateImunisasi(String id) => "$baseUrl/edit-imunisasi/$id";

  //user
  static String postLogin() => "$baseUrl/Login";
  static String updateKader(int id) => "$baseUrl/edit-profile/$id";

  //other
  static String getDesa() => "$baseUrl/getDesa";
  static String getVaksinasi() => "$baseUrl/getVaksinasi";
}
