part of "package:inpos/models/anak.dart";

class AnakController extends Anak {
  static Future<List<Anak>> getDataAnak(int id) async {
    final response = await http.get(ApiService.getAnak(id));
    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse.map((anak) => Anak.fromJson(anak)).toList();
    } else {
      throw Exception('Tidak konek API');
    }
  }

  static Future<List<Anak>> getDetailAnak(int id) async {
    String baseUrl = ApiService.getDetailAnak(id);
    var result = await http.get(baseUrl);
    List jsonObject = json.decode(result.body);
    return jsonObject.map((anak) => Anak.fromJson(anak)).toList();
  }

  static Future createAnak(
    BuildContext ctx,
    String nik,
    String namaAnak,
    String tglLahir,
    String jk,
    String kia,
    String imd,
    String anakKe,
    String pbl,
    String bbl,
    String noKK,
    String nikAyah,
    String nikIbu,
    String namaAyah,
    String namaIbu,
    String noTelp,
    int desa,
    int posyandu,
    String ekonomi,
    String alamat,
  ) async {
    ProgressDialog progressDialog = ProgressDialog(ctx);
    progressDialog.style(message: "Loading");
    progressDialog.show();

    final response = await http.post(ApiService.postAnak(), body: {
      'nik': nik,
      'nama_anak': namaAnak,
      'tgl_lahir': tglLahir,
      'jk': jk,
      'kia': kia,
      'imd': imd,
      'anak_ke': anakKe,
      'pb_lahir': pbl,
      'bb_lahir': bbl,
      'no_kk': noKK,
      'nik_ayah': nikAyah,
      'nik_ibu': nikIbu,
      'nama_ayah': namaAyah,
      'nama_ibu': namaIbu,
      'no_telp': noTelp,
      'status_ekonomi': ekonomi,
      'id_desa': desa.toString(),
      'id_posyandu': posyandu.toString(),
      'alamat': alamat,
    }, headers: {
      'Accept': 'application/json'
    });
    progressDialog.hide();

    if (response.statusCode == 200) {
      Alert(
        context: ctx,
        title: "Data anak berhasil disimpan",
        style: Style.alertSuccess,
        type: AlertType.success,
      ).show();
      Timer(Duration(seconds: 1), () {
        Get.offAll(
          MainPage(indexCurrent: 1),
          transition: Transition.topLevel,
          duration: Duration(seconds: 1),
        );
      });
    }
  }

  static Future updateAnak(
    BuildContext ctx,
    int id,
    String nik,
    String namaAnak,
    String tglLahir,
    String jk,
    String kia,
    String imd,
    String anakKe,
    String pbl,
    String bbl,
    String noKK,
    String nikAyah,
    String nikIbu,
    String namaAyah,
    String namaIbu,
    String noTelp,
    int desa,
    int posyandu,
    String ekonomi,
    String alamat,
  ) async {
    ProgressDialog progressDialog = ProgressDialog(ctx);
    progressDialog.style(message: "Loading");
    progressDialog.show();

    final response = await http.put(ApiService.updateAnak(id), body: {
      'nik': nik,
      'nama_anak': namaAnak,
      'tgl_lahir': tglLahir,
      'jk': jk,
      'kia': kia,
      'imd': imd,
      'anak_ke': anakKe,
      'pb_lahir': pbl,
      'bb_lahir': bbl,
      'no_kk': noKK,
      'nik_ayah': nikAyah,
      'nik_ibu': nikIbu,
      'nama_ayah': namaAyah,
      'nama_ibu': namaIbu,
      'no_telp': noTelp,
      'status_ekonomi': ekonomi,
      'id_desa': desa.toString(),
      'id_posyandu': posyandu.toString(),
      'alamat': alamat,
    }, headers: {
      'Accept': 'application/json'
    });
    progressDialog.hide();

    if (response.statusCode == 200) {
      Alert(
        context: ctx,
        title: "Data anak berhasil diubah",
        style: Style.alertSuccess,
        type: AlertType.success,
      ).show();
      Timer(Duration(seconds: 1), () {
        Get.offAll(
          MainPage(indexCurrent: 1),
          transition: Transition.topLevel,
          duration: Duration(seconds: 1),
        );
      });
    }
  }
}
