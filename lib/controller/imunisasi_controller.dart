part of "package:inpos/models/imunisasi.dart";

class ImunisasiController extends Imunisasi {
  static Future<List<Imunisasi>> getDataImunisasi(int idPosyandu) async {
    final response = await http.get(ApiService.getImunisasi(idPosyandu));
    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse
          .map((imunisasi) => Imunisasi.fromJson(imunisasi))
          .toList();
    } else {
      throw Exception('Tidak konek API');
    }
  }

  static Future<List<Imunisasi>> getDetailImunisasi(String id) async {
    String baseUrl = ApiService.getDetailImun(id);
    var result = await http.get(baseUrl);
    List jsonObject = json.decode(result.body);
    return jsonObject
        .map((imunisasi) => Imunisasi.fromJson(imunisasi))
        .toList();
  }

  static Future createImunisasi(
      BuildContext ctx, int idAnak, int idVaksinasi) async {
    ProgressDialog progressDialog = ProgressDialog(ctx);
    progressDialog.style(message: "Loading");
    progressDialog.show();

    final response = await http.post(
      ApiService.postImunisasi(),
      body: {
        'id_anak': idAnak.toString(),
        'id_vaksinasi': idVaksinasi.toString(),
      },
      headers: {'Accept': 'application/json'},
    );
    progressDialog.hide();

    if (response.statusCode == 200) {
      Alert(
        context: ctx,
        title: "Data imunisasi berhasil ditambah",
        style: Style.alertSuccess,
        type: AlertType.success,
      ).show();
      Timer(Duration(seconds: 1), () {
        Get.offAll(
          MainPage(indexCurrent: 2),
          transition: Transition.rightToLeft,
          duration: Duration(seconds: 1),
        );
      });
    }
  }

  static Future updateImunisasi(
      BuildContext ctx, String id, int idAnak, int idVaksinasi) async {
    ProgressDialog progressDialog = ProgressDialog(ctx);
    progressDialog.style(message: "Loading");
    progressDialog.show();

    final response = await http.put(
      ApiService.updateImunisasi(id),
      body: {
        'id_anak': idAnak.toString(),
        'id_vaksinasi': idVaksinasi.toString(),
      },
      headers: {'Accept': 'application/json'},
    );
    progressDialog.hide();

    if (response.statusCode == 200) {
      Alert(
        context: ctx,
        title: "Data imunisasi berhasil diubah",
        style: Style.alertSuccess,
        type: AlertType.success,
      ).show();
      Timer(Duration(seconds: 1), () {
        Get.offAll(
          MainPage(indexCurrent: 2),
          transition: Transition.zoom,
          duration: Duration(seconds: 1),
        );
      });
    }
  }
}
