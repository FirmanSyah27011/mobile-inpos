part of "package:inpos/models/gizi.dart";

class GiziController extends Gizi {
  static Future<List<Gizi>> getGizi(int id) async {
    final response = await http.get(ApiService.getGizi(id));
    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse.map((gizi) => Gizi.fromJson(gizi)).toList();
    } else {
      throw Exception('Tidak konek API');
    }
  }

  static Future<List<Gizi>> getDetailGizi(String id) async {
    String baseUrl = ApiService.getDetailGizi(id);
    var result = await http.get(baseUrl);
    List jsonObject = json.decode(result.body);
    return jsonObject.map((gizi) => Gizi.fromJson(gizi)).toList();
  }

  static Future createGizi(BuildContext ctx, int id, String pbTb, String bb,
      String asi, String vit, String val, int idPosyandu) async {
    ProgressDialog progressDialog = ProgressDialog(ctx);
    progressDialog.style(message: "Loading");
    progressDialog.show();
    String date = DateFormat("yyyy-MM-dd").format(DateTime.now());
    final response = await http.post(
      ApiService.postGizi(),
      body: {
        'id_anak': id.toString(),
        'tgl_periksa': date,
        'pb_tb': pbTb,
        'bb': bb,
        'asi_eks': asi,
        'id_posyandu': idPosyandu.toString(),
        'vit_a': vit,
        'validasi': val
      },
      headers: {'Accept': 'application/json'},
    );
    print(response.body);
    progressDialog.hide();

    if (response.statusCode == 200) {
      Alert(
        context: ctx,
        title: "Data gizi berhasil disimpan",
        style: Style.alertSuccess,
        type: AlertType.success,
      ).show();
      Timer(Duration(seconds: 1), () {
        Get.offAll(
          MainPage(indexCurrent: 3),
          transition: Transition.zoom,
          duration: Duration(seconds: 1),
        );
      });
    }
  }

  static Future updateGizi(BuildContext ctx, String idGizi, int idAnak,
      String tb, String bb, String asi, String vit, String val) async {
    ProgressDialog progressDialog = ProgressDialog(ctx);
    progressDialog.style(message: "Loading");
    progressDialog.show();

    final response = await http.put(
      ApiService.updateGizi(idGizi),
      body: {
        'id_anak': idAnak.toString(),
        'pb_tb': tb,
        'bb': bb,
        'asi_eks': asi,
        'vit_a': vit,
        'validasi': val
      },
      headers: {'Accept': 'application/json'},
    );
    progressDialog.hide();
    print(response.body);

    if (response.statusCode == 200) {
      Alert(
        context: ctx,
        title: "Data gizi berhasil diubah",
        style: Style.alertSuccess,
        type: AlertType.success,
      ).show();

      Timer(Duration(seconds: 1), () {
        Get.offAll(
          MainPage(indexCurrent: 3),
          transition: Transition.zoom,
          duration: Duration(seconds: 1),
        );
      });
    }
  }
}
