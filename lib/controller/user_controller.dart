import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:inpos/views/styles/style.dart';
import 'package:inpos/services/api_service.dart';
import 'package:inpos/views/pages/login.dart';
import 'package:inpos/views/pages/main_page.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:inpos/helper/preferences.dart';

class UserController {
  static Future login(BuildContext ctx, String usr, String password) async {
    if (usr.isEmpty || password.isEmpty) {
      Alert(
        context: ctx,
        title: "Username dan Password tidak boleh kosong",
        type: AlertType.error,
        style: Style.alertStyle,
      ).show();
      return;
    }

    ProgressDialog progressDialog = ProgressDialog(ctx);
    progressDialog.style(message: "Loading");
    progressDialog.show();

    try {
      final response = await http.post(ApiService.postLogin(), body: {
        'username': usr,
        'password': password
      }, headers: {
        'Accept': 'application/json'
      }).timeout(const Duration(seconds: 10));
      final data = jsonDecode(response.body);
      int error = data['error'];
      progressDialog.hide();

      if (error == 0) {
        Preferences.savePref(
          data['user']['id_user'],
          data['user']['username'],
          data['user']['name'],
          data['user']['jk'],
          data['user']['alamat'],
          data['user']['posyandu']['id_posyandu'],
          data['user']['posyandu']['nama_posyandu'],
          data['user']['posyandu']['puskesmas']['nama_puskesmas'],
          data['user']['posyandu']['puskesmas']['alamat'],
        );

        Alert(
          context: ctx,
          title: "Login berhasil",
          style: Style.alertSuccess,
          type: AlertType.success,
        ).show();

        Timer(Duration(seconds: 1), () {
          Get.offAll(
            MainPage(),
            transition: Transition.rightToLeft,
            duration: Duration(seconds: 1),
          );
        });
      } else if (error == 1) {
        Alert(
          context: ctx,
          title: "Username dan Password salah!",
          style: Style.alertStyle,
          type: AlertType.error,
        ).show();
      } else {
        Alert(
          context: ctx,
          title: "Cek koneksi anda!",
          style: Style.alertStyle,
          type: AlertType.error,
        ).show();
      }
    } on TimeoutException catch (e) {
      print(e);
      progressDialog.hide();
      Alert(
        context: ctx,
        title: "Cek koneksi anda!",
        style: Style.alertStyle,
        type: AlertType.error,
      ).show();
    } on SocketException catch (e) {
      print(e);
      progressDialog.hide();
      Alert(
        context: ctx,
        title: "Cek koneksi anda!",
        style: Style.alertStyle,
        type: AlertType.error,
      ).show();
    } on Error catch (e) {
      print(e);
      Alert(
        context: ctx,
        title: "Username dan Password salah!",
        style: Style.alertStyle,
        type: AlertType.error,
      ).show();
    }
  }

  static Future updateProfile(BuildContext ctx, int id, String username,
      String name, String jk, String alamat) async {
    ProgressDialog progressDialog = ProgressDialog(ctx);
    progressDialog.style(message: "Loading");
    progressDialog.show();
    final response = await http.put(ApiService.updateKader(id), body: {
      'username': username,
      'name': name,
      'jk': jk,
      'alamat': alamat,
    }, headers: {
      'Accept': 'application/json'
    });
    progressDialog.hide();

    if (response.statusCode == 200) {
      Alert(
        context: ctx,
        title: "Data akun berhasil diubah",
        style: Style.alertSuccess,
        type: AlertType.success,
      ).show();
      Preferences.updatePref(username, name, jk, alamat);
      Timer(Duration(seconds: 1), () {
        Get.offAll(
          MainPage(indexCurrent: 4),
          transition: Transition.zoom,
          duration: Duration(seconds: 1),
        );
      });
    }
  }

  static Future logout() async {
    Preferences.destroyPref();
    Get.offAll(
      Login(),
      transition: Transition.leftToRight,
      duration: Duration(seconds: 1),
    );
  }
}
