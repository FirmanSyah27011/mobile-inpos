import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inpos/views/pages/anak/form_add_anak.dart';

void main() {
  testWidgets("add anak with data empty", (WidgetTester tester) async {
    await tester.pumpWidget(FormAddAnak());
    await tester.tap(find.text("Simpan Data"));
    await tester.pump();
    final Finder form = find.byType(Form);
    Form formAdd = tester.widget(form) as Form;
    GlobalKey<FormState> formKey = formAdd.key as GlobalKey<FormState>;
    expect(formKey.currentState.validate(), isFalse);
  });
}
