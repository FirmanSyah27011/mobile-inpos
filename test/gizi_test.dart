import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../lib/views/pages/gizi/form_add_gizi.dart';

void main() {
  testWidgets("add gizi with data empty", (WidgetTester tester) async {
    await tester.pumpWidget(FormAddGizi());
    await tester.tap(find.text('Simpan Data'));
    final Finder form = find.byType(Form);
    Form formAdd = tester.widget(form) as Form;
    GlobalKey<FormState> formKey = formAdd.key as GlobalKey<FormState>;
    await tester.pump();
    expect(formKey.currentState.validate(), isFalse);
  });
}
