import 'package:flutter_test/flutter_test.dart';
import 'package:inpos/views/pages/imunisasi/form_add_imunisasi.dart';

void main() {
  testWidgets("add imunisasi with data empty", (WidgetTester tester) async {
    await tester.pumpWidget(FormAddImunisasi());
    await tester.tap(find.text('Simpan Data'));
    String expRAnak = 'Data anak tidak boleh kosong';
    String expRVaksin = 'Data vaksin tidak boleh kosong';
    await tester.pump();
    expect(find.text(expRAnak), findsOneWidget);
    expect(find.text(expRVaksin), findsOneWidget);
  });
}
